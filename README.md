# 20220320-ErmanMaris-NYCSchools

## Tools
Xcode 13.1 - iOS 15.0

## Architecture 
MVVM

## Project info
There 2 main ViewControllers:

NYCSchoolsListViewController() : 
Contains a tableView and uses UITableViewDiffableDataSource to display schools and makes request for 20 schools during each paginated request.
It has a floating view to show action sheet. By using action sheet, users can filter each school by borough and each filtered list also paginated

NYCShoolDetailViewController() :
Selected school data passed to NYCShoolDetailViewController and school location displayed on a map.
Also, different kinds of schools specific info displayed in a tableView.
Before displaying the whole school data, SAT call is made and SAT data is appended in main array along with other school info
School websites can be tappable and content displayed on Safari.
