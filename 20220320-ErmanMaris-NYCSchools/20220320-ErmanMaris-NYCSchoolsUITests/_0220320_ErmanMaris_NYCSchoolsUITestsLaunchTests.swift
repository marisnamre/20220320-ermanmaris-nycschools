//
//  _0220320_ErmanMaris_NYCSchoolsUITestsLaunchTests.swift
//  20220320-ErmanMaris-NYCSchoolsUITests
//
//  Created by Erman Maris on 3/20/22.
//

import XCTest

class _0220320_ErmanMaris_NYCSchoolsUITestsLaunchTests: XCTestCase {

    override class var runsForEachTargetApplicationUIConfiguration: Bool {
        true
    }

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    func testLaunch() throws {
        let app = XCUIApplication()
        app.launch()

        // Insert steps here to perform after app launch but before taking a screenshot,
        // such as logging into a test account or navigating somewhere in the app

        let attachment = XCTAttachment(screenshot: app.screenshot())
        attachment.name = "Launch Screen"
        attachment.lifetime = .keepAlways
        add(attachment)
    }
}
