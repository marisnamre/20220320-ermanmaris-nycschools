//
//  UITableViewCell+Extension.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/20/22.
//

import Foundation
import UIKit

public extension UITableViewCell {
    static func cellIdentifier() -> String {
        return String(describing: self)
    }
}
