//
//  UIView+Extension.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/20/22.
//

import Foundation
import UIKit

extension UIView {
    func setDefaultAnchors(for view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([view.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
                                     view.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
                                     view.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor),
                                     view.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor)])
    }
    
    func setMapViewAnchors(for view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([view.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                                     view.topAnchor.constraint(equalTo: self.topAnchor),
                                     view.trailingAnchor.constraint(equalTo: self.trailingAnchor),
                                     view.heightAnchor.constraint(equalToConstant: self.frame.height * 0.35)])
    }
}
