//
//  Constants.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/20/22.
//

import Foundation
import UIKit

struct Constants {
    
    struct NYCSchoolsListVM {
        static let appToken = "cfLG4xeYVREHDD9oFFvP6HWME"
        static let pageLimit = 20
        static let pageOffset = 20
        static let pageTitle = "NYC Schools"
    }
    
    struct NYCSchoolsListVC {
        static let defaultPadding: CGFloat = 16.0
        static let filterViewSize: CGFloat = 35.0
    }
    
    struct NYCSchoolDetailVC {
        static let pageTitle = "School detail"
        static let rowHeight: CGFloat = 100.0
    }
    
    struct NYCSchoolDetailVM {
        static let schoolNameTitle = "School Name:"
        static let boroughTitle = "Borough:"
        static let requirementsTitle = "Requirements:"
        static let admissionTitle = "Admission Priorities:"
        static let academicTitle = "Academic Opportunities:"
        static let phoneTitle = "Phone Number:"
        static let webTitle = "Website:"
        static let satTakersTitle = "# of SAT TAkers:"
        static let readingTitle = "Critical Reading avg Score:"
        static let mathTitle = "Math avg Score:"
        static let writingTitle = "Writing avg Score:"
        static let noInfo = "No Info"
    }
}
