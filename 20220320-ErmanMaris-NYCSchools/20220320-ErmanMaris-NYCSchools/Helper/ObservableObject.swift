//
//  ObservableObject.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/20/22.
//

import Foundation

public class ObservableObject<T> {
    public typealias Listener = (T) -> Void
    public var listener: Listener?
    
    public init(_ observableValue: T) {
        self.observableValue = observableValue
    }
    
    public var observableValue: T {
        didSet {
            listener?(observableValue)
        }
    }
    
    public func bind(listener: Listener?) {
        self.listener = listener
        listener?(observableValue)
    }
}
