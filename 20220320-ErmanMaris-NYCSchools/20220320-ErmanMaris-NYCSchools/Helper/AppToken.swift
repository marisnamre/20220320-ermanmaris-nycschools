//
//  AppToken.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/21/22.
//

import Foundation

struct AppToken {
    static func getToken() -> String {
        return "?$$app_token=\(Constants.NYCSchoolsListVM.appToken)"
    }
}
