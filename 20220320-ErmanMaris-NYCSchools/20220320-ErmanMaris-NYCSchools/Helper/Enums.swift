//
//  Enums.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/20/22.
//

import Foundation

public enum NYCSchoolsUrl: CustomStringConvertible {
    case schoolList
    case schoolSAT
    
    public var description: String {
        switch self {
        case .schoolList:
            return "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        case .schoolSAT:
            return "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        }
    }
}

enum Section: CaseIterable {
    case main
}

public enum NYCSchoolsBoro: CustomStringConvertible {
    case allNYC
    case manhattan
    case bronx
    case queens
    case brooklyn
    case statenIsland
    
    public var description: String {
        switch self {
        case .allNYC:
            return "All NYC Schools"
        case .manhattan:
            return "Manhattan"
        case .bronx:
            return "Bronx"
        case .queens:
            return "Queens"
        case .brooklyn:
            return "Brooklyn"
        case .statenIsland:
            return "Staten Island"
        }
    }
    
    public var filter: String {
        switch self {
        case .allNYC:
            return ""
        case .manhattan:
            return "M"
        case .bronx:
            return "X"
        case .queens:
            return "Q"
        case .brooklyn:
            return "K"
        case .statenIsland:
            return "R"
        }
    }
}
