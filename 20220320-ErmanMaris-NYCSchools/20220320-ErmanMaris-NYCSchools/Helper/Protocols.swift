//
//  Protocols.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/20/22.
//

import Foundation

// GlobalCellUsable is used to store different types in the same array that conforms this protocol
protocol GlobalCellUsable {}

protocol NYCSchoolCellConfigurable {
    var schoolName: String? { get set }
    var overviewParagraph: String? { get set }
    var location: String? { get set }
    var id: UUID { get set }
}
