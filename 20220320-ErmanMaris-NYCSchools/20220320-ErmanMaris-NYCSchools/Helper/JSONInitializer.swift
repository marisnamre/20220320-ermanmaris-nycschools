//
//  JSONInitializer.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/21/22.
//

import Foundation

struct JSONInitializer {
    // For initializing mock data to use in unit testing
    static func load<T: Codable>(type: T.Type, file: String) -> T? {
        if let path = Bundle.main.path(forResource: file, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(T.self, from: data)
                return jsonData
            } catch let error {
                print("Parsing error: \(error.localizedDescription)")
            }
        } else {
            print("Invalid filename / path.")
        }
        return nil
    }
}
