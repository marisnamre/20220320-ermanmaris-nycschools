//
//  GlobalInteractor.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/20/22.
//

import Foundation


public class GlobalInteractor {
    
    // Made this one generic so that any model conforms to Codable can be passed here.
    // For now, will just modify it for .GET but it can be configured to support all kinds of requests
    public class func requestData<T: Codable>(type: T.Type, urlString: String, completion: @escaping (_ data: T?, _ error: String?) -> ()) {
        
        guard let url = URL(string: urlString) else {
            return
        }
                
        let urlRequest = URLRequest(url: url)
        
        let dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if let error = error {
                completion(nil, error.localizedDescription)
                return
            }
            
            // Here a range of statusCodes can be checked, for now, I am only checking 200
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completion(nil, "Response error")
                return
            }
            
            guard let data = data else {
                completion(nil, "No data received")
                return
            }
                        
            let decoder = JSONDecoder()
            
            do {
                let mainData = try decoder.decode(type.self, from: data)
                completion(mainData, nil)
                return
            } catch {
                completion(nil, "Decoding error")
                print(error)
                return
            }
        }
        dataTask.resume()
    }
}
