//
//  SATInfoDataModel.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/21/22.
//

import Foundation

public struct SATInfoDataModel: Codable {

    public var dbn: String?
    public var schoolName: String?
    public var numberOfSATTakers: String?
    public var criticalReading: String?
    public var mathScore: String?
    public var writingScore: String?

    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numberOfSATTakers = "num_of_sat_test_takers"
        case criticalReading = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"

    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        dbn = try container.decodeIfPresent(String.self, forKey: .dbn)
        schoolName = try container.decodeIfPresent(String.self, forKey: .schoolName)
        numberOfSATTakers = try container.decodeIfPresent(String.self, forKey: .numberOfSATTakers)
        criticalReading = try container.decodeIfPresent(String.self, forKey: .criticalReading)
        mathScore = try container.decodeIfPresent(String.self, forKey: .mathScore)
        writingScore = try container.decodeIfPresent(String.self, forKey: .writingScore)
    }
}
