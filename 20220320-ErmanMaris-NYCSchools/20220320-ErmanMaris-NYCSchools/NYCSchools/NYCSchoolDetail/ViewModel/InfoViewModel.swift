//
//  InfoViewModel.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/21/22.
//

import Foundation

struct InfoViewModel: GlobalCellUsable {
    let title: String?
    let detail: String
}
