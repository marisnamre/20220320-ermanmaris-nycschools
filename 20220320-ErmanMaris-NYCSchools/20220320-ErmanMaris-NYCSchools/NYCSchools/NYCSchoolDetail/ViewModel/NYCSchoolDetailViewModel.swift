//
//  NYCSchoolDetailViewModel.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/20/22.
//

import Foundation

class NYCSchoolDetailViewModel {
    
    var schoolDetail: NYCSchoolsDataModel?
    var schoolInfo = [GlobalCellUsable]()
    private(set) var notifyVC = ObservableObject<Bool>(false)
    private var satInfo: SATInfoDataModel?
    
    func configure(data: NYCSchoolsDataModel?) {
        self.schoolDetail = data
    }
    
    // Create an array from school info and display them in a tableview
    func prepareInfoArray() {
        
        // InfoViewModel - conforms GlobalCellUsable
        let schoolName = InfoViewModel(title: Constants.NYCSchoolDetailVM.schoolNameTitle,
                                       detail: schoolDetail?.schoolName ?? Constants.NYCSchoolDetailVM.noInfo)
        let borough = InfoViewModel(title: Constants.NYCSchoolDetailVM.boroughTitle,
                                    detail: schoolDetail?.borough ?? Constants.NYCSchoolDetailVM.noInfo)
        let requirements = InfoViewModel(title: Constants.NYCSchoolDetailVM.requirementsTitle, detail: getRequirements())
        let priorities = InfoViewModel(title: Constants.NYCSchoolDetailVM.admissionTitle,detail: getAdmissionPriority())
        let opportunities = InfoViewModel(title: Constants.NYCSchoolDetailVM.academicTitle,detail: getAcademicOpportunities())
        let phoneNumber = InfoViewModel(title: Constants.NYCSchoolDetailVM.phoneTitle,
                                        detail: schoolDetail?.phoneNumber ?? Constants.NYCSchoolDetailVM.noInfo)
        let satTakers = InfoViewModel(title: Constants.NYCSchoolDetailVM.satTakersTitle,
                                      detail: satInfo?.numberOfSATTakers ?? Constants.NYCSchoolDetailVM.noInfo)
        let criticalReading = InfoViewModel(title: Constants.NYCSchoolDetailVM.readingTitle,
                                            detail: satInfo?.criticalReading ?? Constants.NYCSchoolDetailVM.noInfo)
        let mathScore = InfoViewModel(title: Constants.NYCSchoolDetailVM.mathTitle,
                                      detail: satInfo?.mathScore ?? Constants.NYCSchoolDetailVM.noInfo)
        let writingScore = InfoViewModel(title: Constants.NYCSchoolDetailVM.writingTitle,
                                         detail: satInfo?.writingScore ?? Constants.NYCSchoolDetailVM.noInfo)
        
        // InfoWebViewModel - conforms GlobalCellUsable
        let webSite = InfoWebViewModel(title: Constants.NYCSchoolDetailVM.webTitle, url: schoolDetail?.website ?? Constants.NYCSchoolDetailVM.noInfo)
        
        schoolInfo = [schoolName,
                      borough,
                      requirements,
                      priorities,
                      opportunities,
                      satTakers,
                      criticalReading,
                      mathScore,
                      writingScore,
                      phoneNumber,
                      webSite]
        
    }
    
    func requestSATs() {
        
        let urlString = "\(NYCSchoolsUrl.schoolSAT.description)\(getDBN())"
        print("url: \(urlString)")
        
        GlobalInteractor.requestData(type: [SATInfoDataModel].self, urlString: urlString) { [weak self] data, error in
            guard let strongSelf = self else { return }
            
            if let error = error {
                print("\(error)")
                strongSelf.prepareInfoArray()
                strongSelf.notifyVC.observableValue = true
                return
            }
            
            if let satData = data?.first {
                strongSelf.satInfo = satData
            }
            
            strongSelf.prepareInfoArray()
            strongSelf.notifyVC.observableValue = true
        }
    }
    
    func getDBN() -> String {
        return "?dbn=\(schoolDetail?.dbn ?? "")"
    }
    
    // Combine Multiple requirements and add them into a String
    private func getRequirements() -> String {
        let requirements = "\(schoolDetail?.requirementOneOne ?? "")\(schoolDetail?.requirementTwoOne ?? "")\(schoolDetail?.requirementThreeOne ?? "")\(schoolDetail?.requirementFourOne ?? "")\(schoolDetail?.requirementFiveOne ?? "")"
        return requirements.isEmpty ? Constants.NYCSchoolDetailVM.noInfo : requirements
    }
    
    // Combine Multiple academic opportunities and add them into a String
    private func getAcademicOpportunities() -> String {
        let opportunities = "\(schoolDetail?.academicopportunitiesOne ?? "")\(schoolDetail?.academicopportunitiesTwo ?? "")"
        return opportunities.isEmpty ? Constants.NYCSchoolDetailVM.noInfo : opportunities
    }
    
    // Combine Multiple admission priorities and add them into a String
    private func getAdmissionPriority() -> String {
        let priorities = "\(schoolDetail?.admissionsPriorityOne ?? "")\(schoolDetail?.admissionsPriorityTwo ?? "")\(schoolDetail?.admissionsPriorityThree ?? "")"
        return priorities.isEmpty ? Constants.NYCSchoolDetailVM.noInfo : priorities
    }
    
    func getCount() -> Int {
        return schoolInfo.count
    }
}
