//
//  InfoWebViewModel.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/21/22.
//

import Foundation

struct InfoWebViewModel: GlobalCellUsable {
    let title: String?
    let url: String?
}

extension InfoWebViewModel {
    
    func modify() -> String {
        
        // All websites missing "https://"
        // and some "www"
        // So I modify them here
        var modifiedURL = String()
        
        if let containsWWW = url?.contains("www"), containsWWW {
            modifiedURL = "https://\(url ?? "")"
        } else {
            modifiedURL =  "https://www.\(url ?? "")"
        }
        
        return modifiedURL
    }
}
