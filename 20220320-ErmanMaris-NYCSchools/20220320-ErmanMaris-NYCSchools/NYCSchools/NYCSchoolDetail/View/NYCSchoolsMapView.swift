//
//  NYCSchoolsMapView.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/20/22.
//

import UIKit
import MapKit

// Map gets School's coordinates, centers, and puts a pin
class NYCSchoolsMapView: UIView {
    private var mapView = MKMapView(frame: .zero)
    private var school: NYCSchoolsDataModel?
    
    func configure(data: NYCSchoolsDataModel?) {
        self.school = data
        setupMapViewAnchors()
        setupMapView()
    }
    
    private func setupMapViewAnchors() {
        self.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([mapView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                                     mapView.topAnchor.constraint(equalTo: self.topAnchor),
                                     mapView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
                                     mapView.bottomAnchor.constraint(equalTo: self.bottomAnchor)])
    }
    
    private func setupMapView() {
        let lat = convert(location: school?.lat)
        let long = convert(location: school?.long)
        
        let initialLocation = CLLocation(latitude: lat, longitude: long)
        let coordinateRegion = MKCoordinateRegion( center: initialLocation.coordinate,
                                                   latitudinalMeters: 750,
                                                   longitudinalMeters: 750)
        mapView.setRegion(coordinateRegion, animated: true)
        
       placeAnnotation(lat: lat, long: long)
    }
    
    private func placeAnnotation(lat: Double, long: Double) {
        let schoolAnnotation = NYCSchoolAnnotation(
            title: school?.schoolName,
            locationName: school?.city,
            coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long))
        mapView.addAnnotation(schoolAnnotation)
    }
    
    private func convert(location: String?) -> Double {
        if let location = location, let convertedValue = Double(location) {
            return convertedValue
        }
        
        return 0.0
    }
    
}

