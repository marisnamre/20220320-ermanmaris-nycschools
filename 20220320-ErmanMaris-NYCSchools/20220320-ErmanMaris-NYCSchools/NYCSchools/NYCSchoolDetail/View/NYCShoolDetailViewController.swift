//
//  NYCShoolDetailViewController.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/20/22.
//

import UIKit

class NYCShoolDetailViewController: UIViewController {
    
    private var mapView = NYCSchoolsMapView()
    private var viewModel = NYCSchoolDetailViewModel()
    private var tableView = UITableView(frame: .zero)
    
    convenience init(school: NYCSchoolsDataModel?) {
        self.init()
        viewModel.configure(data: school)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = Constants.NYCSchoolDetailVC.pageTitle
        view.backgroundColor = .white
        registerObservableValue()
        setupMapView()
        setupTableView()
        viewModel.requestSATs()
    }
    
    private func setupMapView() {
        view.addSubview(mapView)
        mapView.configure(data: viewModel.schoolDetail)
        view.setMapViewAnchors(for: mapView)
    }
    
    private func setupTableView() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                                     tableView.topAnchor.constraint(equalTo: mapView.bottomAnchor),
                                     tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                                     tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)])
        
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Constants.NYCSchoolDetailVC.rowHeight
        tableView.register(UINib(nibName: NYCSchoolsTextViewTableViewCell.cellIdentifier(), bundle: nil), forCellReuseIdentifier: NYCSchoolsTextViewTableViewCell.cellIdentifier())
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: UITableViewCell.cellIdentifier())
    }
    
    private func registerObservableValue() {
        viewModel.notifyVC.bind { [weak self] (_ value) in
            guard let strongSelf = self else { return }
            DispatchQueue.main.async {
                strongSelf.tableView.reloadData()
            }
        }
    }
}

extension NYCShoolDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.getCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // InfoWebViewModel for displaying websites
        if let viewModel = viewModel.schoolInfo[indexPath.row] as? InfoWebViewModel {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: NYCSchoolsTextViewTableViewCell.cellIdentifier(), for: indexPath) as? NYCSchoolsTextViewTableViewCell else { return UITableViewCell() }
            cell.delegate = self
            cell.configure(data: viewModel)
            return cell
        }
        
        // InfoViewModel for the other school info
        if let viewModel = viewModel.schoolInfo[indexPath.row] as? InfoViewModel {
            let cell = tableView.dequeueReusableCell(withIdentifier: UITableViewCell.cellIdentifier(), for: indexPath)
            var content = cell.defaultContentConfiguration()
            content.text = viewModel.title
            content.secondaryText = viewModel.detail
            cell.contentConfiguration = content
            return cell
        }
        
        return UITableViewCell()
    }
}

extension NYCShoolDetailViewController: NYCSchoolsTextViewTableViewCellDelegate {
    // Open url in Safari
    func linkHasBeenTapped(url: URL) {
        DispatchQueue.main.async {
            UIApplication.shared.open(url)
        }
    }
}
