//
//  NYCSchoolsTextViewTableViewCell.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/21/22.
//

import UIKit

protocol NYCSchoolsTextViewTableViewCellDelegate: AnyObject {
    func linkHasBeenTapped(url: URL)
}

// Cell for displaying website data and open it
class NYCSchoolsTextViewTableViewCell: UITableViewCell, UITextViewDelegate {
    
    @IBOutlet weak var infoTitle: UILabel!
    @IBOutlet weak var textView: UITextView!
    weak var delegate: NYCSchoolsTextViewTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textView.delegate = self
        textView.isScrollEnabled = false
        textView.isEditable = false
        self.selectionStyle = .none
    }
    
    func configure(data: InfoWebViewModel?) {
        infoTitle.text = data?.title
        
        let modifiedURL = data?.modify() ?? ""
        
        let attributedString = NSMutableAttributedString(string: modifiedURL)
        attributedString.addAttribute(.link, value: modifiedURL, range: NSRange(location: 0, length: modifiedURL.count))
        
        textView.attributedText = attributedString
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        delegate?.linkHasBeenTapped(url: URL)
        return false
    }
}
