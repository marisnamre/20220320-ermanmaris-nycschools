//
//  NYCSchoolsDataModel.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/20/22.
//

import Foundation

public struct NYCSchoolsDataModel: Codable, Hashable, GlobalCellUsable, NYCSchoolCellConfigurable {

    public var dbn: String?
    public var schoolName: String?
    public var overviewParagraph: String?
    public var school10thSeats: String?
    public var academicopportunitiesOne: String?
    public var academicopportunitiesTwo: String?
    public var neighborhood: String?
    public var location: String?
    public var phoneNumber: String?
    public var website: String?
    public var subway: String?
    public var bus: String?
    public var requirementOneOne: String?
    public var requirementTwoOne: String?
    public var requirementThreeOne: String?
    public var requirementFourOne: String?
    public var requirementFiveOne: String?
    public var OfferRate: String?
    public var primaryAddressLine: String?
    public var city: String?
    public var zip: String?
    public var stateCode: String?
    public var lat: String?
    public var long: String?
    public var borough: String?
    public var admissionsPriorityOne: String?
    public var admissionsPriorityTwo: String?
    public var admissionsPriorityThree: String?
    public var id: UUID = UUID()

    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case school10thSeats = "school_10th_seats"
        case academicopportunitiesOne = "academicopportunities1"
        case academicopportunitiesTwo = "academicopportunities2"
        case neighborhood
        case location
        case phoneNumber = "phone_number"
        case website
        case subway
        case bus
        case requirementOneOne = "requirement1_1"
        case requirementTwoOne = "requirement2_1"
        case requirementThreeOne = "requirement3_1"
        case requirementFourOne = "requirement4_1"
        case requirementFiveOne = "requirement5_1"
        case OfferRate = "offer_rate1"
        case primaryAddressLine = "primary_address_line_1"
        case city
        case zip
        case stateCode = "state_code"
        case lat = "latitude"
        case long = "longitude"
        case borough
        case admissionsPriorityOne = "admissionspriority11"
        case admissionsPriorityTwo = "admissionspriority21"
        case admissionsPriorityThree = "admissionspriority31"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        dbn = try container.decodeIfPresent(String.self, forKey: .dbn)
        schoolName = try container.decodeIfPresent(String.self, forKey: .schoolName)
        overviewParagraph = try container.decodeIfPresent(String.self, forKey: .overviewParagraph)
        school10thSeats = try container.decodeIfPresent(String.self, forKey: .school10thSeats)
        academicopportunitiesOne = try container.decodeIfPresent(String.self, forKey: .academicopportunitiesOne)
        academicopportunitiesTwo = try container.decodeIfPresent(String.self, forKey: .academicopportunitiesTwo)
        neighborhood = try container.decodeIfPresent(String.self, forKey: .neighborhood)
        location = try container.decodeIfPresent(String.self, forKey: .location)
        phoneNumber = try container.decodeIfPresent(String.self, forKey: .phoneNumber)
        website = try container.decodeIfPresent(String.self, forKey: .website)
        subway = try container.decodeIfPresent(String.self, forKey: .subway)
        bus = try container.decodeIfPresent(String.self, forKey: .bus)
        requirementOneOne = try container.decodeIfPresent(String.self, forKey: .requirementOneOne)
        requirementTwoOne = try container.decodeIfPresent(String.self, forKey: .requirementTwoOne)
        requirementThreeOne = try container.decodeIfPresent(String.self, forKey: .requirementThreeOne)
        requirementFourOne = try container.decodeIfPresent(String.self, forKey: .requirementFourOne)
        requirementFiveOne = try container.decodeIfPresent(String.self, forKey: .requirementFiveOne)
        OfferRate = try container.decodeIfPresent(String.self, forKey: .OfferRate)
        primaryAddressLine = try container.decodeIfPresent(String.self, forKey: .primaryAddressLine)
        city = try container.decodeIfPresent(String.self, forKey: .city)
        zip = try container.decodeIfPresent(String.self, forKey: .zip)
        stateCode = try container.decodeIfPresent(String.self, forKey: .stateCode)
        lat = try container.decodeIfPresent(String.self, forKey: .lat)
        long = try container.decodeIfPresent(String.self, forKey: .long)
        borough = try container.decodeIfPresent(String.self, forKey: .borough)
        admissionsPriorityOne = try container.decodeIfPresent(String.self, forKey: .admissionsPriorityOne)
        admissionsPriorityTwo = try container.decodeIfPresent(String.self, forKey: .admissionsPriorityTwo)
        admissionsPriorityThree = try container.decodeIfPresent(String.self, forKey: .admissionsPriorityThree)
    }
}

extension NYCSchoolsDataModel {
    // Create full address from different properties
    func getLocation() -> String {
        let primaryAddressLine = primaryAddressLine ?? ""
        let city = city ?? ""
        let stateCode = stateCode ?? ""
        let zip = zip ?? ""
        return "\(primaryAddressLine)" + " " + "\(city)" + " " + "\(stateCode)" + " " + "\(zip)"
    }
}

extension NYCSchoolsDataModel: Equatable {
    public static func == (lhs: NYCSchoolsDataModel, rhs: NYCSchoolsDataModel) -> Bool {
        return lhs.id == rhs.id
    }
}
