//
//  NYCSchoolsFilterView.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/21/22.
//

import UIKit

protocol NYCSchoolsFilterViewDelegate: AnyObject {
    func filterHasBeenTapped()
}

// Floating view on Schools list screen
class NYCSchoolsFilterView: UIView {

    private var filterIcon = UIImageView()
    weak var delegate: NYCSchoolsFilterViewDelegate?
    
    func configure() {
        self.addSubview(filterIcon)
        filterIcon.frame = self.bounds
        filterIcon.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        filterIcon.image = UIImage(named: "filterImage")
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(triggerTap))
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc private func triggerTap() {
        delegate?.filterHasBeenTapped()
    }
    
}
