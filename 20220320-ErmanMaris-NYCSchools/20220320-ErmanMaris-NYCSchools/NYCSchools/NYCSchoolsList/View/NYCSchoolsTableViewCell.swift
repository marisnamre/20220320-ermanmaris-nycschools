//
//  NYCSchoolsTableViewCell.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/20/22.
//

import UIKit

protocol NYCSchoolsTableViewCellDelegate: AnyObject {
    func selected(school: NYCSchoolsDataModel)
}

class NYCSchoolsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var overviewParagraph: UILabel!
    @IBOutlet weak var location: UILabel!
    private var id: UUID?
    weak var delegate: NYCSchoolsTableViewCellDelegate?
    private var activeSchool: NYCSchoolsDataModel?
    
    func configure(data: NYCSchoolsDataModel) {
        setupCell()
        activeSchool = data
        
        schoolName.text = data.schoolName
        overviewParagraph.text = data.overviewParagraph
        location.text = "Address:\n\(data.getLocation())"
        id = data.id
    }
    
    // id will be nil during init and set the config by calling setupCell()
    // if id not nil, its recycled cell and it wont set config once more
    private func setupCell() {
        if id == nil {
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(cellBeenTapped))
            self.addGestureRecognizer(tapGestureRecognizer)
            self.selectionStyle = .none
        }
    }
    
    @objc private func cellBeenTapped() {
        if let school = activeSchool {
            delegate?.selected(school: school)
        }
    }
}
