//
//  NYCSchoolsListViewController.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/20/22.
//

import UIKit

typealias NYCSchoolsDataSource = UITableViewDiffableDataSource<Section, NYCSchoolsDataModel>

class NYCSchoolsListViewController: UIViewController {
    
    private var viewModel = NYCSchoolsListViewModel()
    private let tableView = UITableView(frame: .zero)
    var dataSource: NYCSchoolsDataSource?
    private var filterView = NYCSchoolsFilterView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = Constants.NYCSchoolsListVM.pageTitle
        registerObservableValue()
        viewModel.requestNYCShoolList()
        setupTableView()
        setupTableViewDataSource()
        setupFilterView()
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: NYCSchoolsTableViewCell.cellIdentifier(), bundle: nil), forCellReuseIdentifier:   NYCSchoolsTableViewCell.cellIdentifier())
        tableView.delegate = self
        view.addSubview(tableView)
        view.setDefaultAnchors(for: tableView)
    }
    
    func setupFilterView() {
        view.addSubview(filterView)
        filterView.configure()
        filterView.delegate = self
        filterView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([filterView.heightAnchor.constraint(equalToConstant: Constants.NYCSchoolsListVC.filterViewSize),
                                     filterView.widthAnchor.constraint(equalToConstant: Constants.NYCSchoolsListVC.filterViewSize),
                                     filterView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Constants.NYCSchoolsListVC.defaultPadding),
                                     filterView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -Constants.NYCSchoolsListVC.defaultPadding)])
        
    }
    
    func setupTableViewDataSource() {
        dataSource = NYCSchoolsDataSource(tableView: tableView, cellProvider: { tableView, indexPath, school in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: NYCSchoolsTableViewCell.cellIdentifier(), for: indexPath) as? NYCSchoolsTableViewCell else { return UITableViewCell() }
            cell.delegate = self
            cell.configure(data: school)
            return cell
        })
    }
    
    private func registerObservableValue() {
        viewModel.notifyVC.bind { [weak self] (_ value) in
            guard let strongSelf = self else { return }
            strongSelf.load(data: strongSelf.viewModel.nycSchoolsData)
        }
    }
    
    private func load(data:  [NYCSchoolsDataModel]) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, NYCSchoolsDataModel>()
        snapshot.appendSections([.main])
        snapshot.appendItems(data)
        dataSource?.apply(snapshot, animatingDifferences: true)
    }
}

extension NYCSchoolsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // Request new data when tableView reaches to the last element
        if let lastElement = viewModel.nycSchoolsData.last {
            if lastElement.id == viewModel.nycSchoolsData[indexPath.row].id {
                viewModel.requestNYCShoolList()
            }
        }
    }
}

// Pass selected school to detail VC
extension NYCSchoolsListViewController: NYCSchoolsTableViewCellDelegate {
    func selected(school: NYCSchoolsDataModel) {
        let schoolDetailVC = NYCShoolDetailViewController(school: school)
        navigationController?.pushViewController(schoolDetailVC, animated: true)
    }
}

// ActionSheet to filter schools by borough
extension NYCSchoolsListViewController: NYCSchoolsFilterViewDelegate {
    func filterHasBeenTapped() {
        
        let filterMenu = UIAlertController(title: nil, message: "Filter Schools", preferredStyle: .actionSheet)
        
        let allNYC = UIAlertAction(title: NYCSchoolsBoro.allNYC.description, style: .default) { [weak self] _ in
            print("\(NYCSchoolsBoro.allNYC.description) tapped")
            self?.viewModel.makeAFilteredCall(for: NYCSchoolsBoro.allNYC.filter)
        }
        
        let manhattan = UIAlertAction(title: NYCSchoolsBoro.manhattan.description, style: .default) { [weak self] _ in
            print("\(NYCSchoolsBoro.manhattan.description) tapped")
            self?.viewModel.makeAFilteredCall(for: NYCSchoolsBoro.manhattan.filter)
        }
        
        let bronx = UIAlertAction(title: NYCSchoolsBoro.bronx.description, style: .default) { [weak self] _ in
            print("\(NYCSchoolsBoro.bronx.description) tapped")
            self?.viewModel.makeAFilteredCall(for: NYCSchoolsBoro.bronx.filter)
        }
        
        let queens = UIAlertAction(title: NYCSchoolsBoro.queens.description, style: .default) { [weak self] _ in
            print("\(NYCSchoolsBoro.queens.description) tapped")
            self?.viewModel.makeAFilteredCall(for: NYCSchoolsBoro.queens.filter)
            
        }
        
        let brooklyn = UIAlertAction(title: NYCSchoolsBoro.brooklyn.description, style: .default) { [weak self] _ in
            print("\(NYCSchoolsBoro.brooklyn.description) tapped")
            self?.viewModel.makeAFilteredCall(for: NYCSchoolsBoro.brooklyn.filter)
        }
        
        let statenIsland = UIAlertAction(title: NYCSchoolsBoro.statenIsland.description, style: .default) { [weak self] _ in
            print("\(NYCSchoolsBoro.statenIsland.description) tapped")
            self?.viewModel.makeAFilteredCall(for: NYCSchoolsBoro.statenIsland.filter)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel)
        
        filterMenu.addAction(allNYC)
        filterMenu.addAction(manhattan)
        filterMenu.addAction(bronx)
        filterMenu.addAction(queens)
        filterMenu.addAction(brooklyn)
        filterMenu.addAction(statenIsland)
        filterMenu.addAction(cancel)
        
        self.present(filterMenu, animated: true, completion: nil)
    }
}
