//
//  NYCSchoolsListViewModel.swift
//  20220320-ErmanMaris-NYCSchools
//
//  Created by Erman Maris on 3/20/22.
//

import Foundation

class NYCSchoolsListViewModel {
    
    private(set) var nycSchoolsData = [NYCSchoolsDataModel]()
    private(set) var notifyVC = ObservableObject<Bool>(false)
    
    var pageOffset = 0
    var isLoading = false
    // Passing borough filter that holds filter letter passed from VC
    var boroFilter = String()
    
    func requestNYCShoolList() {
        guard !isLoading else { return }

        isLoading = true
        let urlString = "\(NYCSchoolsUrl.schoolList.description)\(AppToken.getToken())\(getPageParameters())\(getBoroParameters())"
        print("URL: \(urlString)")
        
        GlobalInteractor.requestData(type: [NYCSchoolsDataModel].self, urlString: urlString) { [weak self] data, error in
            guard let strongSelf = self else { return }
            
            if let error = error {
                print("\(error)")
                return
            }
            
            if let schoolData = data {
                strongSelf.assign(data: schoolData)
            }
            
            strongSelf.isLoading = false
            strongSelf.pageOffset += Constants.NYCSchoolsListVM.pageOffset
        }
    }
    
    func assign(data: [NYCSchoolsDataModel]) {
        guard !data.isEmpty else { return }
        
        for school in data {
            nycSchoolsData.append(school)
        }
        
        notifyVC.observableValue = true
    }
    
    // For pagination
    func getPageParameters() -> String {
        return"&$limit=\(Constants.NYCSchoolsListVM.pageLimit)&$offset=\(pageOffset)"
    }
    
    // getBoroParameters is used to add filter to the url
    func getBoroParameters() -> String {
        // if no boto filter set, just return empty this way, it will make a call for all schools in NYC
        return boroFilter.isEmpty ? "" : "&boro=\(boroFilter)"
    }
    
    func makeAFilteredCall(for boro: String) {
        // reset data and make a new request for filtered schools
        self.boroFilter = boro
        nycSchoolsData = []
        pageOffset = 0
        requestNYCShoolList()
    }
}
