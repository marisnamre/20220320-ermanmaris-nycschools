//
//  _0220320_ErmanMaris_NYCSchoolsTests.swift
//  20220320-ErmanMaris-NYCSchoolsTests
//
//  Created by Erman Maris on 3/20/22.
//

import XCTest
@testable import _0220320_ErmanMaris_NYCSchools

class _0220320_ErmanMaris_NYCSchoolsTests: XCTestCase {
    
    var viewModel = NYCSchoolsListViewModel()
    var detailViewModel = NYCSchoolDetailViewModel()
    let mockJSONSchool = JSONInitializer.load(type: NYCSchoolsDataModel.self, file: "School")

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testFetchSchoolList() throws {
        
        let promise = expectation(description: "Fetch!")
        let urlString = "\(NYCSchoolsUrl.schoolList.description)\(AppToken.getToken())\(viewModel.getPageParameters())"
        GlobalInteractor.requestData(type: [NYCSchoolsDataModel].self, urlString: urlString) { data, error in
            
            if let error = error {
                print("\(error)")
                XCTFail("Fetch error: \(error.description)")
                return
            }
            
            XCTAssertNotNil(data)
            promise.fulfill()
            
        }
        wait(for: [promise], timeout: 2)
    }
    
    func testFetchSATScores() throws {
        
        let promise = expectation(description: "Fetch!")
        let urlString = "\(NYCSchoolsUrl.schoolSAT.description)\(detailViewModel.getDBN())"
        
        GlobalInteractor.requestData(type: [SATInfoDataModel].self, urlString: urlString) { data, error in
            
            if let error = error {
                print("\(error)")
                XCTFail("Fetch error: \(error.description)")
                return
            }
            
            XCTAssertNotNil(data)
            promise.fulfill()
            
        }
        wait(for: [promise], timeout: 2)
    }
    
    func testSchoolName() throws {
        XCTAssertEqual(mockJSONSchool?.schoolName, "Clinton School Writers & Artists, M.S. 260")
    }
    
    func testWebSite() throws {
        let infoWebVM = InfoWebViewModel(title: "Website", url: mockJSONSchool?.website)
        let modifiedWebsite = infoWebVM.modify()
        XCTAssertEqual(modifiedWebsite, "https://www.theclintonschool.net")
    }
    
    func testSchoolDetailInfo() throws {
        detailViewModel.configure(data: mockJSONSchool)
        detailViewModel.prepareInfoArray()
        XCTAssertTrue(!detailViewModel.schoolInfo.isEmpty)
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
